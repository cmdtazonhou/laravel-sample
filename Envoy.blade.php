@servers(['web' => 'deployer@94.247.176.97'])

@setup
    $repository = 'git@gitlab.com:cmdtazonhou/laravel-sample.git';
    $app_dir = '/var/www/vhosts/dmdsatis.com/laravel-gitlab-cicd.dmdsatis.com';
    $releases_dir = '/var/www/vhosts/dmdsatis.com/laravel-gitlab-cicd.dmdsatis.com/releases';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
    chmod -R 775 bootstrap/cache
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    /opt/plesk/php/7.3/bin/php /usr/lib/plesk-9.0/composer.phar install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    rm -rf {{ $app_dir }}/current
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask
